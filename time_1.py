import signal
import time


class TimedOutExc(Exception):
    """
    Raised when a timeout happens
    """
    
    
def timeout(timeout):
    """
    Return a decorator that raises a TimedOutExc exception
    after timeout seconds, if the decorated function did not return.
    """
    def decorate(f):
        def handler(signum, frame):
            raise TimedOutExc()
        
        def new_f(*args, **kwargs):
            old_handler = signal.signal(signal.SIGALRM, handler)
            signal.alarm(timeout)
            result = f(*args, **kwargs)  # f() always returns, in this scheme
            signal.signal(signal.SIGALRM, old_handler)  # Old signal handler is restored
            signal.alarm(0)  # Alarm removed
            return result
        # new_f.func_name = f.func_name
        return new_f
    return decorate


@timeout(6)
def function_that_takes_a_long_time(num):
    try:
        time.sleep(num)
        print('ok')
        # ... long, parallel calculation ...
    except TimedOutExc as e:
        print('error is {}'.format(str(e)))
        # ... Code that shuts down the processes ...
        # ...
        # return None  # Or exception raised, which means that the calculation is not complete
        
        
if __name__ == '__main__':
    list1 = [4, 8]
    for i in list1:
        
        function_that_takes_a_long_time(i)

from threading import Timer
from functools import wraps
from datetime import datetime
import time


def log_time_delta(func):
    @wraps(func)
    def deco():
        start = datetime.now()
        res = func()
        end = datetime.now()
        delta = end - start
        print("func runed ", delta)
        return res
    return deco


def time_limit(interval):

    def deco(func):
        def time_out():
            raise TimeoutError()

        @wraps(func)
        def inner(*args, **kwargs):
            timer = Timer(interval, time_out)
            timer.start()
            func(*args, **kwargs)
            timer.cancel()
        return inner
    return deco


@time_limit(3)
def t():
    time.sleep(1)
    print('do not time out')
    print(t.__doc__)
    print(t.__name__)
    return None


@time_limit(3)
def tl(num):
    time.sleep(num)
    return None


if __name__ == '__main__':
    for i in range(2, 6):
        
        tl(i)
